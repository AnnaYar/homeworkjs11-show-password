
//     Завдання

/*Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.

Технічні вимоги:
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів 
користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, 
саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.*/


const passwordInputs = document.querySelectorAll('.password-input');
const icons = document.querySelectorAll('.icon-password');

icons.forEach((icon) => {
    icon.onclick = function() {
        const passwordInput = icon.parentElement.querySelector('.password-input');
        if (passwordInput.type === 'password') {
            passwordInput.type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else {
            passwordInput.type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    };
});
        
const form = document.querySelector('form');
    form.onsubmit = function (event){
        event.preventDefault();
        const pass1 = document.querySelector('#password1');
        const pass2 = document.querySelector('#password2');

    if (pass1.value === pass2.value) {
        alert('You are welcome');
        pass1.value = '';
        pass2.value = '';
    } else {
        const passForm = document.querySelector('.password-form');
        const errorText = document.createElement('p');
        errorText.innerText = 'Потрібно ввести однакові значення';
        errorText.classList.add("error-text");
        passForm.insertBefore(errorText, passForm.querySelector('.btn'));
        pass1.value = '';
        pass2.value = '';
    }
};